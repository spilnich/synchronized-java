package concurrency;

public class Swapper {

    private String name1 = "Вася";
    private volatile static String name2 = "Петя";

    public void swap() {

        synchronized (this) {

            String s = name1;
            name1 = name2;
            name2 = s;
        }
    }
}
