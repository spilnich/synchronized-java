package concurrency;

public class Printer {

    public static void main(String[] args) {
        new MyThread().start();
        new MyThread().start();
    }

    public static class TestSynchronization {

        public static void print(int n) {        //method not synchronized

            for (int i = 1; i <= 5; i++) {
                System.out.println(n * i);
                try {
                    Thread.sleep(400);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class MyThread extends Thread {
        @Override
        public void run() {
            TestSynchronization.print(5);
        }
    }
}



